from setuptools import find_packages, setup

with open("README.md", "r") as fh:
    long_description = fh.read()

setup(
    name="zoomrlib",
    version="1.0.0",
    description="Library to read and write a ZoomR16 project file",
    author="Rémy Taymans",
    author_email="remytms@tsmail.eu",
    url="https://gitlab.com/remytms/zoomrlib",
    license="GPL-3.0+",
    long_description=long_description,
    long_description_content_type="text/markdown",
    package_dir={"": "src"},  # Set src as root for finding packages
    packages=["zoomrlib"],
    install_requires=[],
    entry_points={"console_scripts": ["zoomrlib = zoomrlib.cli:main"]},
    provides=["zoomrlib"],
    python_requires=">=3.6",
    classifiers=[
        "Intended Audience :: Developers",
        "Development Status :: 5 - Production/Stable"
        "License :: OSI Approved "
        ":: GNU General Public License v3 or later (GPLv3+)",
        "Operating System :: Unix",
        "Operating System :: POSIX",
        "Operating System :: Microsoft :: Windows",
        "Programming Language :: Python",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: 3.7",
        "Programming Language :: Python :: 3.8",
        "Programming Language :: Python :: Implementation :: CPython",
        "Topic :: Multimedia :: Sound/Audio",
        "Topic :: Software Development :: Libraries",
    ],
    project_urls={
        "Changelog": (
            "https://gitlab.com/remytms/zoomrlib/blob/master/CHANGES.md"
        ),
        "Issue Tracker": "https://gitlab.com/remytms/zoomrlib/issues",
    },
)
