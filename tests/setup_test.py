"""Test if setup and special var in zoomrlib are the same."""

import re

import zoomrlib


def test_setup():
    """Test setup.py file."""
    values = {
        "name": zoomrlib.__productname__,
        "version": zoomrlib.__version__,
        "description": zoomrlib.__description__,
        "author": zoomrlib.__author__,
        "author_email": zoomrlib.__author_email__,
        "url": zoomrlib.__url__,
        "license": zoomrlib.__license__,
    }
    with open("setup.py") as file:
        match = re.search(r"setup\((.*)\)", file.read(), re.S)
        setup = match.group(1)
    setup_lines = setup.split(",\n")
    for line in setup_lines:
        match = re.search(r"(\w+)=(.+)", line, re.S)
        if match:
            key, py_expr = match.group(1, 2)
            if key in values:
                # pylint: disable=eval-used
                assert values[key] == eval(py_expr)
