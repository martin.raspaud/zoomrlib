# Copyright 2019-2020 Rémy Taymans <remytms@tsmail.eu>
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl.html).

"""Test for zoomrlib library"""

from pathlib import Path

import pytest

import zoomrlib

__testdir__ = Path(__file__).parent


def test_open_raises():
    """Test open function"""
    with pytest.raises(TypeError):
        zoomrlib.open("filename", 123)
    with pytest.raises(ValueError):
        zoomrlib.open("filename", "rw")
        zoomrlib.open("filename", "rb")
        zoomrlib.open("filename", "r+")
        zoomrlib.open("filename", "t")
        zoomrlib.open("filename", "a")


def test_dump_raises():
    """Test dump function"""
    with pytest.raises(TypeError):
        zoomrlib.dump("", None)


def test_project_init():
    """Test project creation"""
    with pytest.raises(TypeError):
        zoomrlib.Project("")
    empty_project = zoomrlib.Project()
    assert empty_project.name == zoomrlib.Project.default_name
    prjname = __testdir__ / "data" / "PRJDATA.ZDT"
    with open(prjname, "rb") as file:
        project = zoomrlib.Project(bytearray(file.read()))
    assert project.name == "PRJ028"


def test_default_project():
    """Test creation of a default project"""
    prjname = __testdir__ / "data" / "test_default.zdt"
    project = zoomrlib.Project()

    with zoomrlib.open(prjname, "w") as file:
        zoomrlib.dump(project, file)

    with zoomrlib.open(prjname, "r") as file:
        project = zoomrlib.load(file)
    assert project.header == project.default_header
    assert project.name == project.default_name
    assert project.bitlength == project.default_bitlength
    assert project.protected == project.default_protected
    assert project.insert_effect_on == project.default_insert_effect_on
    for track in project.tracks:
        assert track.file == track.default_file
        assert track.status == track.default_status
        assert track.stereo_on == track.default_stereo_on
        assert track.invert_on == track.default_invert_on
        assert track.pan == track.default_pan
        assert track.fader == track.default_fader
        assert track.chorus_on == track.default_chorus_on
        assert track.chorus_gain == track.default_chorus_gain
        assert track.reverb_on == track.default_reverb_on
        assert track.reverb_gain == track.default_reverb_gain
        assert track.eqhigh_on == track.default_eqhigh_on
        assert track.eqhigh_freq == track.default_eqhigh_freq
        assert track.eqhigh_gain == track.default_eqhigh_gain
        assert track.eqmid_on == track.default_eqmid_on
        assert track.eqmid_freq == track.default_eqmid_freq
        assert track.eqmid_qfactor == track.default_eqmid_qfactor
        assert track.eqmid_gain == track.default_eqmid_gain
        assert track.eqlow_on == track.default_eqlow_on
        assert track.eqlow_freq == track.default_eqlow_freq
        assert track.eqlow_gain == track.default_eqlow_gain
    assert project.master.file == project.master.default_file
    assert project.master.fader == project.master.default_fader
    prjname.unlink()


def test_default_values_project():
    """
    Test that default values corresponds to the default value of the
    device.
    """
    prjname = __testdir__ / "data" / "DEFAULT.ZDT"

    with zoomrlib.open(prjname, "r") as file:
        project = zoomrlib.load(file)
    assert project.header == project.default_header
    # Project name of this project file is "PROJ001", which is not the
    # default value for a project name. A default project name should be
    # "PROJ000". So this value is not tested here.
    assert project.bitlength == project.default_bitlength
    assert project.protected == project.default_protected
    assert project.insert_effect_on == project.default_insert_effect_on
    for track in project.tracks:
        assert track.file == track.default_file
        assert track.status == track.default_status
        assert track.stereo_on == track.default_stereo_on
        assert track.invert_on == track.default_invert_on
        assert track.pan == track.default_pan
        assert track.fader == track.default_fader
        assert track.chorus_on == track.default_chorus_on
        assert track.chorus_gain == track.default_chorus_gain
        assert track.reverb_on == track.default_reverb_on
        assert track.reverb_gain == track.default_reverb_gain
        assert track.eqhigh_on == track.default_eqhigh_on
        assert track.eqhigh_freq == track.default_eqhigh_freq
        assert track.eqhigh_gain == track.default_eqhigh_gain
        assert track.eqmid_on == track.default_eqmid_on
        assert track.eqmid_freq == track.default_eqmid_freq
        assert track.eqmid_qfactor == track.default_eqmid_qfactor
        assert track.eqmid_gain == track.default_eqmid_gain
        assert track.eqlow_on == track.default_eqlow_on
        assert track.eqlow_freq == track.default_eqlow_freq
        assert track.eqlow_gain == track.default_eqlow_gain
    assert project.master.file == project.master.default_file
    assert project.master.fader == project.master.default_fader


def test_project():
    """Test Project with PRJDATA.ZDT file"""
    prjname = __testdir__ / "data" / "PRJDATA.ZDT"
    with open(prjname, "rb") as file:
        project = zoomrlib.load(file)
    # Project
    assert project.name == "PRJ028"
    assert project.header == "ZOOM R-16  PROJECT DATA VER0001"
    assert not project.protected
    assert project.bitlength == 16
    # Track 1
    assert project.tracks[0].file == "MONO-000.WAV"
    assert not project.tracks[0].stereo_on
    assert not project.tracks[0].invert_on
    assert project.tracks[0].pan == 50
    assert project.tracks[0].fader == 47
    assert project.tracks[0].chorus_on
    assert project.tracks[0].chorus_gain == 0
    assert project.tracks[0].reverb_on
    assert project.tracks[0].reverb_gain == 10
    assert project.tracks[0].eqhigh_on
    assert project.tracks[0].eqmid_on
    assert project.tracks[0].eqlow_on
    # Track 2
    assert project.tracks[1].file == "MONO-001.WAV"
    assert not project.tracks[1].stereo_on
    assert not project.tracks[1].invert_on
    assert project.tracks[1].pan == -50
    assert project.tracks[1].fader == 45
    assert project.tracks[1].chorus_on
    assert project.tracks[1].chorus_gain == 0
    assert project.tracks[1].reverb_on
    assert project.tracks[1].reverb_gain == 10
    assert project.tracks[1].eqhigh_on
    assert project.tracks[1].eqmid_on
    assert project.tracks[1].eqlow_on
    # Track 3
    assert project.tracks[2].file == "MONO-002.WAV"
    assert not project.tracks[2].stereo_on
    assert not project.tracks[2].invert_on
    assert project.tracks[2].pan == 0
    assert project.tracks[2].fader == 87
    assert project.tracks[2].chorus_on
    assert project.tracks[2].chorus_gain == 0
    assert project.tracks[2].reverb_on
    assert project.tracks[2].reverb_gain == 0
    assert project.tracks[2].eqhigh_on
    assert project.tracks[2].eqmid_on
    assert project.tracks[2].eqlow_on
    # Track 4
    assert project.tracks[3].file == "MONO-003.WAV"
    assert not project.tracks[3].stereo_on
    assert not project.tracks[3].invert_on
    assert project.tracks[3].pan == -15
    assert project.tracks[3].fader == 75
    assert project.tracks[3].chorus_on
    assert project.tracks[3].chorus_gain == 0
    assert project.tracks[3].reverb_on
    assert project.tracks[3].reverb_gain == 20
    assert project.tracks[3].eqhigh_on
    assert project.tracks[3].eqmid_on
    assert project.tracks[3].eqlow_on
    # Master
    assert project.master.file == "MASTR000.WAV"
    with pytest.raises(AttributeError):
        project.master.pan  # pylint: disable=no-member,pointless-statement
    assert project.master.fader == 98


def test_project2():
    """Test Project with PRJDATA2.ZDT file"""
    prjname = __testdir__ / "data" / "PRJDATA2.ZDT"
    with open(prjname, "rb") as file:
        project = zoomrlib.load(file)
    # Project
    assert project.name == "PRJ006"
    assert project.bitlength == 16
    # EQ High Frequency
    assert project.tracks[0].eqhigh_freq == 1000
    assert project.tracks[1].eqhigh_freq == 1300
    assert project.tracks[2].eqhigh_freq == 1600
    assert project.tracks[3].eqhigh_freq == 2000
    assert project.tracks[4].eqhigh_freq == 2500
    assert project.tracks[5].eqhigh_freq == 3200
    assert project.tracks[6].eqhigh_freq == 4000
    assert project.tracks[7].eqhigh_freq == 5000
    # EQ High Gain
    assert project.tracks[0].eqhigh_gain == 0
    assert project.tracks[1].eqhigh_gain == 0
    assert project.tracks[2].eqhigh_gain == 0
    assert project.tracks[3].eqhigh_gain == 0
    assert project.tracks[4].eqhigh_gain == 0
    assert project.tracks[5].eqhigh_gain == 0
    assert project.tracks[6].eqhigh_gain == 0
    assert project.tracks[7].eqhigh_gain == 0
    # EQ Mid Frequency
    assert project.tracks[0].eqmid_freq == 1000
    assert project.tracks[1].eqmid_freq == 1300
    assert project.tracks[2].eqmid_freq == 1600
    assert project.tracks[3].eqmid_freq == 2000
    assert project.tracks[4].eqmid_freq == 2500
    assert project.tracks[5].eqmid_freq == 3200
    assert project.tracks[6].eqmid_freq == 4000
    assert project.tracks[7].eqmid_freq == 5000
    # EQ Mid Q-Factor
    assert project.tracks[0].eqmid_qfactor == 0.1
    assert project.tracks[1].eqmid_qfactor == 0.2
    assert project.tracks[2].eqmid_qfactor == 0.3
    assert project.tracks[3].eqmid_qfactor == 0.4
    assert project.tracks[4].eqmid_qfactor == 0.5
    assert project.tracks[5].eqmid_qfactor == 0.6
    assert project.tracks[6].eqmid_qfactor == 0.7
    assert project.tracks[7].eqmid_qfactor == 0.8
    # EQ Mid Gain
    assert project.tracks[0].eqmid_gain == -12
    assert project.tracks[1].eqmid_gain == 0
    assert project.tracks[2].eqmid_gain == 12
    assert project.tracks[3].eqlow_gain == 0
    assert project.tracks[4].eqlow_gain == 0
    assert project.tracks[5].eqlow_gain == 0
    assert project.tracks[6].eqlow_gain == 0
    assert project.tracks[7].eqlow_gain == 0
    # EQ Low Frequency
    assert project.tracks[0].eqlow_freq == 40
    assert project.tracks[1].eqlow_freq == 50
    assert project.tracks[2].eqlow_freq == 63
    assert project.tracks[3].eqlow_freq == 80
    assert project.tracks[4].eqlow_freq == 100
    assert project.tracks[5].eqlow_freq == 125
    assert project.tracks[6].eqlow_freq == 160
    assert project.tracks[7].eqlow_freq == 200
    # EQ Low Gain
    assert project.tracks[0].eqlow_gain == 0
    assert project.tracks[1].eqlow_gain == 0
    assert project.tracks[2].eqlow_gain == 0
    assert project.tracks[3].eqlow_gain == 0
    assert project.tracks[4].eqlow_gain == 0
    assert project.tracks[5].eqlow_gain == 0
    assert project.tracks[6].eqlow_gain == 0
    assert project.tracks[7].eqlow_gain == 0


def test_project_stereo():
    """Test Project with STEREO.ZDT file"""
    prjname = __testdir__ / "data" / "STEREO.ZDT"
    with open(prjname, "rb") as file:
        project = zoomrlib.load(file)
    assert project.protected
    assert project.bitlength == 24
    assert project.insert_effect_on
    assert project.tracks[0].stereo_on
    assert project.tracks[0].eqhigh_gain == 5
    assert project.tracks[0].eqmid_gain == 5
    assert project.tracks[0].eqlow_gain == 5
    # The following tracks are linked stereo
    # The first track properties affects the two tracks, but the second
    # tracks keeps its settings in the project file.
    assert project.tracks[0].fader == 102
    assert project.tracks[1].fader == 80


def test_project_green_status():
    """Test Project with GREEN.ZDT file"""
    prjname = __testdir__ / "data" / "GREEN.ZDT"
    with open(prjname, "rb") as file:
        project = zoomrlib.load(file)
    for track in project.tracks:
        assert track.status == "play"


def test_project_mute_status():
    """Test Project with DOWN.ZDT file"""
    prjname = __testdir__ / "data" / "DOWN.ZDT"
    with open(prjname, "rb") as file:
        project = zoomrlib.load(file)
    for track in project.tracks:
        assert track.status == "mute"


def test_project_red_status():
    """Test Project with RED.ZDT file"""
    prjname = __testdir__ / "data" / "RED.ZDT"
    with open(prjname, "rb") as file:
        project = zoomrlib.load(file)
    assert project.tracks[0].status == "record"
    assert project.tracks[1].status == "record"
    assert project.tracks[2].status == "record"
    assert project.tracks[3].status == "record"
    assert project.tracks[4].status == "record"
    assert project.tracks[5].status == "record"
    assert project.tracks[6].status == "record"
    assert project.tracks[7].status == "record"
    assert project.tracks[8].status == "play"
    assert project.tracks[9].status == "play"
    assert project.tracks[10].status == "play"
    assert project.tracks[11].status == "play"
    assert project.tracks[12].status == "play"
    assert project.tracks[13].status == "play"
    assert project.tracks[14].status == "play"
    assert project.tracks[15].status == "play"


def test_project_mixed_status():
    """Test Project with MIX.ZDT file"""
    prjname = __testdir__ / "data" / "MIX.ZDT"
    with open(prjname, "rb") as file:
        project = zoomrlib.load(file)
    assert project.tracks[0].status == "record"
    assert project.tracks[1].status == "play"
    assert project.tracks[2].status == "mute"
    assert project.tracks[3].status == "record"
    assert project.tracks[4].status == "play"
    assert project.tracks[5].status == "mute"
    assert project.tracks[6].status == "record"
    assert project.tracks[7].status == "play"
    assert project.tracks[8].status == "mute"
    assert project.tracks[9].status == "record"
    assert project.tracks[10].status == "play"
    assert project.tracks[11].status == "mute"
    assert project.tracks[12].status == "record"
    assert project.tracks[13].status == "play"
    assert project.tracks[14].status == "mute"
    assert project.tracks[15].status == "record"
