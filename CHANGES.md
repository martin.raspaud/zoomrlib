zoomrlib CHANGES
================


1.0.0 (2020-10-24)
------------------

* Brings a small CLI tool to convert ZDT file to a JSON file.


0.4.0 (2020-10-23)
------------------

* Project, Tracks and Master Track can now be fully written.
* Bring some fixes in file name, when no file name is associated to a
  track.
* Frequencies should be now used as integer not string.


0.3.0 (2020-10-08)
------------------

* Project can now be written
* Some fixes


0.2.0 (2019-12-14)
------------------

* Stereo property is now properly read.
* Read project bitlength and insert_effect_on
* It's now possible to create an empty project, but no write possible
  for now.
* Can read track status (rec/paly/mute)


0.1.0 (2019-12-10)
------------------

* For a project, it reads:
  * Heading
  * Project name
  * Protected on/off
* For the master track, it reads:
  * Associated file
  * Fader volume
* For each track, it reads:
  * Associated file
  * Stereo on/off
  * Invert on/off
  * Pan equalizer
  * Fader volume
  * Chorus on/off
  * Chorus gain
  * Reverb on/off
  * Reverb gain
  * High equalizer on/off
  * High equalizer frequency
  * High equalizer gain
  * Mid equalizer on/off
  * Mid equalizer frequency
  * Mid equalizer q-factor
  * Mid equalizer gain
  * Low equalizer on/off
  * Low equalizer frequency
  * Low equalizer gain
* No CLI yet.
